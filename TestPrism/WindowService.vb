﻿Imports Microsoft.Practices.Unity
Imports Planifab.Infrastructure.Prism
Imports Prism.Regions

Namespace Planifab.BaseFramework


    Public Interface IWindowService

        Function ShowWindow(Of view As Window)(context As Object)
        Function CreateWindow(Of view As Window)() As Window

    End Interface


    Public Class WindowService
        Implements IWindowService

        Private ReadOnly _container As IUnityContainer


        Public Sub New(container As IUnityContainer)
            _container = container
        End Sub



        Public Function ShowWindow(Of view As Window)(context As Object) Implements IWindowService.ShowWindow
            Dim window As Window = Nothing

            Dim scopedContainer As IUnityContainer = _container.CreateChildContainer()
            Dim scopedRegionManager As New RegionManagerContainerAware(scopedContainer)
            scopedContainer.RegisterInstance(scopedRegionManager)
            scopedContainer.RegisterInstance(context)

            window = _container.Resolve(GetType(view))
            RegionManager.SetRegionManager(window, scopedRegionManager)


            window.Show()
            Return window
        End Function


        Public Function CreateWindow(Of view As Window)() As Window Implements IWindowService.CreateWindow
            Dim window As Window = _container.Resolve(GetType(view))
            Return window
        End Function

    End Class

End Namespace
