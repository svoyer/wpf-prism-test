﻿Imports Prism.Unity
Imports Microsoft.Practices.Unity
Imports TestPrism.Planifab.BaseFramework
Imports Prism.Regions
Imports Prism.Modularity
Imports Planifab.Infrastructure.Prism
Imports Infrastructure.AvalonDock

Public Class Bootstrapper
    Inherits UnityBootstrapper


    Private windowService As IWindowService


    Protected Overrides Function CreateShell() As DependencyObject
        Return windowService.CreateWindow(Of MainWindow)
    End Function


    Protected Overrides Sub InitializeShell()
        Application.Current.MainWindow = Shell
        Application.Current.MainWindow.Show()
    End Sub


    Protected Overrides Function ConfigureDefaultRegionBehaviors() As IRegionBehaviorFactory
        Dim behaviors As IRegionBehaviorFactory = MyBase.ConfigureDefaultRegionBehaviors()
        behaviors.AddIfMissing(ScopedDependencyAwareBehavior.BehaviorKey, GetType(ScopedDependencyAwareBehavior))
        Return behaviors
    End Function


    Protected Overrides Sub ConfigureContainer()
        MyBase.ConfigureContainer()

        Container.RegisterType(Of IRegionManager, RegionManagerContainerAware)

        windowService = New WindowService(Container)
        Container.RegisterInstance(windowService)

        Container.RegisterType(Of PaneManager)(New ContainerControlledLifetimeManager)
    End Sub


    Protected Overrides Sub ConfigureModuleCatalog()
        MyBase.ConfigureModuleCatalog()

        Dim ModuleCatalog As ModuleCatalog = Me.ModuleCatalog
        ModuleCatalog.AddModule(GetType(Module1))
    End Sub

End Class
