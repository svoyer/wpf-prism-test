﻿Imports Infrastructure.AvalonDock
Imports Prism.Mvvm


Namespace TestPrism.ViewModel

    Public Class Workspace
        Inherits BindableBase

        Private _title As String
        Public ReadOnly Property PaneManager As PaneManager


        Public Sub New(paneManager As PaneManager)
            _PaneManager = paneManager
            Title = "WPF + Prism + AvalonDock"
        End Sub


        Public Property Title As String
            Get
                Return _title
            End Get
            Set(value As String)
                SetProperty(_title, value)
            End Set
        End Property

    End Class

End Namespace
