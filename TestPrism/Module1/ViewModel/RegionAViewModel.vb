﻿Imports Microsoft.Practices.Unity
Imports Planifab.Infrastructure.Prism
Imports Prism.Events
Imports Prism.Mvvm


Public Class RegionAViewModel
    Inherits BindableBase
    Implements IScopedDependencyAware

    Private _text As String = "Region A"
    Public Property Text As String
        Get
            Return _text
        End Get
        Set(value As String)
            SetProperty(_text, value)
        End Set
    End Property


    Public Sub ScopedDependencyInitialisation(container As IUnityContainer) Implements IScopedDependencyAware.ScopedDependencyInitialisation
        Text = "Region A - With Child Container"

        Dim projet As Project = container.Resolve(Of Project)
        Dim scopedEventAggregator As EventAggregator = container.Resolve(Of EventAggregator)
        Dim projectFormCommand As ProjectFormCommand = container.Resolve(Of ProjectFormCommand)
    End Sub

End Class
