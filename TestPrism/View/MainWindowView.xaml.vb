﻿Imports TestPrism.TestPrism.ViewModel


Class MainWindow

    Public Sub New(viewModel As Workspace)

        ' Cet appel est requis par le concepteur.
        InitializeComponent()

        ' Ajoutez une initialisation quelconque après l'appel InitializeComponent().
        DataContext = viewModel
    End Sub

End Class
