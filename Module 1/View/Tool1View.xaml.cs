﻿using Module1.ViewModel;


namespace Module1.View
{
    /// <summary>
    /// Logique d'interaction pour Tool1View.xaml
    /// </summary>
    public partial class Tool1View
    {
        public Tool1View(Tool1ViewModel viewModel)
        {
            InitializeComponent();

            DataContext = viewModel;
        }
    }
}