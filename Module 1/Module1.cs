﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using Infrastructure.AvalonDock;
using Module1.View;


namespace Module1
{

    public class Module1 : IModule
    {
        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _container;
        private readonly PaneManager _paneManager;


        public Module1(IUnityContainer container, IRegionManager regionManager, PaneManager paneManager)
        {
            _container = container;
            _regionManager = regionManager;
            _paneManager = paneManager;
        }


        public void Initialize()
        {
            RegisterViews();
            RegisterPanes();
        }


        public void RegisterPanes()
        {
            Tool1View view = _container.Resolve<Tool1View>();
            RegionManager.SetRegionManager(view, _regionManager);

            _paneManager.AddPane(view);
        }


        public void RegisterViews()
        {
            _regionManager.RegisterViewWithRegion("RegionA", typeof(RegionAView));
        }
    }

}
