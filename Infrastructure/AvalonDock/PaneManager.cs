﻿using System.Collections.ObjectModel;
using System.Windows.Controls;


namespace Infrastructure.AvalonDock
{
    public class PaneManager
    {

        public ObservableCollection<PaneViewModelAdapter> Documents { get; internal set; }
        public ObservableCollection<PaneViewModelAdapter> Tools { get; internal set; }


        public PaneManager()
        {
            Documents = new ObservableCollection<PaneViewModelAdapter>();
            Tools = new ObservableCollection<PaneViewModelAdapter>();
        }
        

        public void AddPane(ContentControl view)
        {
            PaneViewModelAdapter viewModelAdapter = new PaneViewModelAdapter(view);

            if (viewModelAdapter.ViewModel is ToolViewModel)
                Tools.Add(viewModelAdapter);
            else if (viewModelAdapter.ViewModel is PaneViewModel)
                Documents.Add(viewModelAdapter);
        }

    }
}
