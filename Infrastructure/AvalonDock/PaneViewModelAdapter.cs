﻿using Prism.Mvvm;
using System.Windows.Controls;


namespace Infrastructure.AvalonDock
{
    public class PaneViewModelAdapter : BindableBase
    {
        private ContentControl _view;
        private PaneViewModel _viewModel;

    
        public PaneViewModelAdapter(ContentControl view)
        {
           _view = view;
           _viewModel = view.DataContext as PaneViewModel;
        }

        public ContentControl View
        {
            get
            {
                return _view;
            }
            set
            {
                SetProperty(ref _view, value);
            }
        }

        public PaneViewModel ViewModel
        {
            get
            {
                return _viewModel;
            }
            set
            {
                SetProperty(ref _viewModel, value);
            }
        }
    }

}
