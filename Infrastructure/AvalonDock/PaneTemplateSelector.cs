﻿using System.Windows;
using System.Windows.Controls;


namespace Infrastructure.AvalonDock
{
    public class PanesTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ContentTemplate { get; set; }
        public DataTemplate ToolTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is PaneViewModelAdapter pane)
            {
                if (pane.ViewModel is ToolViewModel)
                    return ToolTemplate;

                if (pane.ViewModel is PaneViewModel)
                    return ContentTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }

}
