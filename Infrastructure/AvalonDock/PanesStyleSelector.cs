﻿using System.Windows;
using System.Windows.Controls;


namespace Infrastructure.AvalonDock
{
    public class PanesStyleSelector : StyleSelector
    {
        public Style ToolStyle { get; set; }
        public Style ContentStyle { get; set; }


        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is PaneViewModelAdapter pane)
            {
                if (pane.ViewModel is ToolViewModel)
                    return ToolStyle;

                if (pane.ViewModel is PaneViewModel)
                    return ContentStyle;
            }

            return base.SelectStyle(item, container);
        }
    }

}
