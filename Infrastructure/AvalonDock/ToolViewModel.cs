﻿

namespace Infrastructure.AvalonDock
{
    public class ToolViewModel : PaneViewModel
    {
        private bool _isVisible = true;


        public ToolViewModel(string title) : base(title)
        {
        }


        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                SetProperty(ref _isVisible, value);
            }
        }
    }

}
