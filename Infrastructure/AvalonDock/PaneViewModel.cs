﻿using Prism.Mvvm;


namespace Infrastructure.AvalonDock
{
    public class PaneViewModel : BindableBase
    {
        private string _title = null;
        private bool _isActive = false;
        private bool _isSelected = false;


        public PaneViewModel(string title)
        {
            _title = title;
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                SetProperty(ref _title, value);
            }
        }

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                SetProperty(ref _isSelected, value);
            }
        }

        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                SetProperty(ref _isActive, value);
            }
        }
    }

}
