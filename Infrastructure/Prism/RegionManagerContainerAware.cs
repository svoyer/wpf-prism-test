﻿using Microsoft.Practices.Unity;
using Prism.Regions;

namespace Planifab.Infrastructure.Prism
{

    public class RegionManagerContainerAware : RegionManager
    {
        public IUnityContainer Container { get; private set; }


        public RegionManagerContainerAware(IUnityContainer container) : base()
        {
            Container = container;
        }

        public RegionManagerContainerAware CreateRegionManager(IUnityContainer container)
        {
            return new RegionManagerContainerAware(container);
        }
    }
}
