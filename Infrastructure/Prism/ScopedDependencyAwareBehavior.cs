﻿using System;
using System.Windows;
using Prism.Regions;


namespace Planifab.Infrastructure.Prism
{
    public class ScopedDependencyAwareBehavior : RegionBehavior
    {
        public const string BehaviorKey = "ScopedDependencyAwareBehavior";

        protected override void OnAttach()
        {

            IRegionManager regionManager = Region.RegionManager;

            foreach (var item in this.Region.Views)
                {

                    FrameworkElement element = item as FrameworkElement;
                    if (element != null)
                    {
                        IRegionManager scopedRegionManager = element.GetValue(RegionManager.RegionManagerProperty) as IRegionManager;
                        if (scopedRegionManager != null)
                            regionManager = scopedRegionManager;
                    }

                    RegionManagerContainerAware regionManagerContainerAware = regionManager as RegionManagerContainerAware;
                    InvokeOnScopedDependencyAwareElement(item, x => x.ScopedDependencyInitialisation(regionManagerContainerAware.Container));
                }
            }



        static void InvokeOnScopedDependencyAwareElement(object item, Action<IScopedDependencyAware> invocation)
        {
            var rmAwareItem = item as IScopedDependencyAware;
            if (rmAwareItem != null)
                invocation(rmAwareItem);

            var frameworkElement = item as FrameworkElement;
            if (frameworkElement != null)
            {
                IScopedDependencyAware rmAwareDataContext = frameworkElement.DataContext as IScopedDependencyAware;
                if (rmAwareDataContext != null)
                {
                    var frameworkElementParent = frameworkElement.Parent as FrameworkElement;
                    if (frameworkElementParent != null)
                    {
                        var rmAwareDataContextParent = frameworkElementParent.DataContext as IScopedDependencyAware;
                        if (rmAwareDataContextParent != null)
                        {
                            if (rmAwareDataContext == rmAwareDataContextParent)
                            {
                                return;
                            }
                        }
                    }

                    invocation(rmAwareDataContext);
                }
            }
        }
    }
}
