﻿using System.Windows;
using Microsoft.Practices.Unity;


namespace Planifab.Infrastructure.Prism
{
    public static class ScopedDependencyAware
    {
        public static void InitialiseDependency(object item, IUnityContainer childContainer)
        {
            var rmAware = item as IScopedDependencyAware;
            if (rmAware != null)
                rmAware.ScopedDependencyInitialisation(childContainer);

            var rmAwareFrameworkElement = item as FrameworkElement;
            if (rmAwareFrameworkElement != null)
            {
                var rmAwareDataContext = rmAwareFrameworkElement.DataContext as IScopedDependencyAware;
                if (rmAwareDataContext != null)
                    rmAwareDataContext.ScopedDependencyInitialisation(childContainer);
            }
        }
    }
}
