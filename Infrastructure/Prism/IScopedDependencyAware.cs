﻿using Microsoft.Practices.Unity;


namespace Planifab.Infrastructure.Prism
{
    public interface IScopedDependencyAware
    {
        void ScopedDependencyInitialisation(IUnityContainer container);
    }
}
